var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var minifyjs = require('gulp-js-minify');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// Tache convertir le scss en css
gulp.task('sass', function() {
    return gulp.src('css/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'));
});
// Verifie à chaque fois le fichier sass
gulp.task('default', function() {
    gulp.watch('css/style.scss', ['sass']);
});

// Tache minifié le css
gulp.task('minify-css', function() {
    // Chemin complet du fichier style.css
    gulp.src('css/*.css')
        .pipe(cssmin())
        .pipe(rename({
            // style.css en style.min.css
            suffix: '.min'
        }))
        // Destination : nom du dossier
        .pipe(gulp.dest('dist/css'));
});

// Tache minifé le javascript
gulp.task('minify-js', function() {
    gulp.src('js/*.js')
        .pipe(minifyjs())
        .pipe(rename({
            // style.css en style.min.css
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist'));
});

// Tache actualisé les pages automatiquement
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    // Actualise toutes les pages à la racine du projet automatiquement après chaque modification + tout ce qui ce trouve dans le dossier test
    gulp.watch("*").on('change', browserSync.reload);
    gulp.watch("test/*").on('change', browserSync.reload);
});